package pl.edu.bit.notifications.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class MemberJson {
    private final String userId;
    private final String email;

    @JsonCreator
    MemberJson(@JsonProperty("userId") String userId, @JsonProperty("email") String email) {
        this.userId = userId;
        this.email = email;
    }
}
