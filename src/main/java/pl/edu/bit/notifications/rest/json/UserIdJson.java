package pl.edu.bit.notifications.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class UserIdJson {
    private final String userId;

    @JsonCreator
    UserIdJson(@JsonProperty("userId") String userId) {
        this.userId = userId;
    }
}
