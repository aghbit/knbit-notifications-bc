package pl.edu.bit.notifications.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import lombok.Value;

import java.util.Collection;
import java.util.Map;

@Value
public class MemberListJson {
    private final Map<String, String> values;

    @JsonCreator
    MemberListJson(@JsonProperty("values") Collection<MemberJson> values) {
        this.values = Maps.newHashMap();
        values.forEach((memberJson -> this.values.put(memberJson.getUserId(), memberJson.getEmail())));
    }
}
