package pl.edu.bit.notifications.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class MailRequestJson {
    private final String subject;
    private final String message;
    private final List<UserIdJson> receivers;

    @JsonCreator
    public MailRequestJson(
            @JsonProperty("subject") String subject,
            @JsonProperty("message") String message,
            @JsonProperty("receivers") List<UserIdJson> receivers) {
        this.subject = subject;
        this.message = message;
        this.receivers = receivers;
    }

}
