package pl.edu.bit.notifications.rest.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.notifications.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.BAD_REQUEST)
public class NoEmailsForGivenIdsException extends Exception {
    private final String reason = "NO_EMAILS_FOR_GIVEN_USER_IDS";
}
