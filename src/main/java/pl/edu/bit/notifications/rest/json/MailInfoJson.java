package pl.edu.bit.notifications.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class MailInfoJson {
    private final long pendingEmailBatchesToProcess;

    @JsonCreator
    public MailInfoJson(@JsonProperty("pendingEmailBatchesToProcess") long pendingEmailBatchesToProcess) {
        this.pendingEmailBatchesToProcess = pendingEmailBatchesToProcess;
    }
}
