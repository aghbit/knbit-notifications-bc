package pl.edu.bit.notifications.rest.controller;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.notifications.core.MailManager;
import pl.edu.bit.notifications.rest.config.InvalidMessageBodyException;
import pl.edu.bit.notifications.rest.exception.ForbiddenException;
import pl.edu.bit.notifications.rest.exception.NoEmailsForGivenIdsException;
import pl.edu.bit.notifications.rest.exception.UnauthorizedException;
import pl.edu.bit.notifications.rest.json.MailInfoJson;
import pl.edu.bit.notifications.rest.json.MailRequestJson;

import javax.validation.Valid;

@RestController
public class MailController {

    @Autowired
    MailManager mailManager;

    @RequestMapping(
            value = "/mail",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void sendMail(@Valid @RequestBody MailRequestJson mailRequest,
                         @NonNull @RequestHeader("knbit-aa-auth") String authToken,
                         @RequestParam(value = "sendToAllBitMembers", required = false, defaultValue = "false")
                         boolean sendToAllBitMembers,
                         @RequestParam(value = "isMockRequest", required = false, defaultValue = "false")
                         boolean isMockRequest)
            throws UnauthorizedException, ForbiddenException, NoEmailsForGivenIdsException, InvalidMessageBodyException {
        if (mailRequest.getReceivers() == null && !sendToAllBitMembers) {
            throw new InvalidMessageBodyException();
        }
        if (!isMockRequest) {
            mailManager.scheduleMail(mailRequest, authToken, sendToAllBitMembers);
        }
    }

    @RequestMapping(
            value = "/mail",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public MailInfoJson checkPendingJobs() {
        return new MailInfoJson(mailManager.getTaskInProgressCount());
    }
}
