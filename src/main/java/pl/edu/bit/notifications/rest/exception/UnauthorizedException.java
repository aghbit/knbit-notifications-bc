package pl.edu.bit.notifications.rest.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.notifications.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends Exception {
    private final String reason = "UNAUTHORIZED";
}
