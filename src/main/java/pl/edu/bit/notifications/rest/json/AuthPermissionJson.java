package pl.edu.bit.notifications.rest.json;

import lombok.Value;

@Value
public class AuthPermissionJson {
    private final String permission;
}
