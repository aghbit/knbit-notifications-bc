package pl.edu.bit.notifications.core.services;

import com.google.common.collect.Lists;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.edu.bit.notifications.core.mail.MailRequest;
import pl.edu.bit.notifications.rest.exception.NoEmailsForGivenIdsException;
import pl.edu.bit.notifications.rest.json.MailRequestJson;
import pl.edu.bit.notifications.rest.json.MemberListJson;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Component
public class MembershipService {

    @Value("${service.membership.url}")
    private String membershipUrl;

    @Value("${service.auth.header}")
    private String authHeader;

    public MailRequest convertIdsToEmails(MailRequestJson mailRequestJson, String authToken, boolean
            sendToAllBitMembers) throws NoEmailsForGivenIdsException {
        MemberListJson memberListJson = getAllMembersList(authToken);
        List<String> memberEmails = (sendToAllBitMembers) ? toMailList(memberListJson) : convert(mailRequestJson, memberListJson);

        return new MailRequest(mailRequestJson.getSubject(), mailRequestJson.getMessage(), memberEmails);
    }

    private MemberListJson getAllMembersList(String authToken) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add(authHeader, authToken);
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<MemberListJson> response = restTemplate.exchange(membershipUrl, HttpMethod.GET, entity,
                MemberListJson.class);
        return response.getBody();
    }


    private List<String> toMailList(MemberListJson memberListJson) {
        return Lists.newLinkedList(memberListJson.getValues().values());
    }

    private List<String> convert(MailRequestJson mailRequestJson, MemberListJson memberListJson) throws NoEmailsForGivenIdsException {
        List<String> membersEmails = mailRequestJson.getReceivers().stream()
                .map((userId -> memberListJson.getValues().get(userId.getUserId())))
                .collect(Collectors.toList());
        if (membersEmails.isEmpty()) {
            throw new NoEmailsForGivenIdsException();
        }
        return membersEmails;
    }

}