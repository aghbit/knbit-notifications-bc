package pl.edu.bit.notifications.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.bit.notifications.core.mail.MailRequest;
import pl.edu.bit.notifications.core.mail.MailSendingExecutor;
import pl.edu.bit.notifications.core.services.AuthService;
import pl.edu.bit.notifications.core.services.MembershipService;
import pl.edu.bit.notifications.rest.exception.ForbiddenException;
import pl.edu.bit.notifications.rest.exception.NoEmailsForGivenIdsException;
import pl.edu.bit.notifications.rest.exception.UnauthorizedException;
import pl.edu.bit.notifications.rest.json.MailRequestJson;

@Component
public class MailManager {

    private final AuthService authService;

    private final MembershipService membershipService;

    private final MailSendingExecutor mailSendingExecutor;

    @Autowired
    MailManager(AuthService authService, MembershipService membershipService, MailSendingExecutor mailSendingExecutor) {
        this.authService = authService;
        this.membershipService = membershipService;
        this.mailSendingExecutor = mailSendingExecutor;
    }

    public void scheduleMail(MailRequestJson mailRequestJson, String authToken, boolean sendToAllBitMembers) throws
            UnauthorizedException, ForbiddenException, NoEmailsForGivenIdsException {
        authService.authorize(authToken);
        MailRequest mailRequest = membershipService.convertIdsToEmails(mailRequestJson, authToken, sendToAllBitMembers);
        mailSendingExecutor.scheduleMail(mailRequest, authToken);
    }

    public long getTaskInProgressCount() {
        return mailSendingExecutor.getTaskInProgressCount();
    }
}
