package pl.edu.bit.notifications.core.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
public class MailSender {

    private final JavaMailSender javaMailSender;

    private final String defaultEncoding;

    private final String mailAuthorAccount;

    @Autowired
    MailSender(JavaMailSender javaMailSender, @Qualifier("mailAuthor") String mailAuthor,
               @Qualifier("defaultEncoding") String defaultEncoding) {
        this.javaMailSender = javaMailSender;
        this.mailAuthorAccount = mailAuthor;
        this.defaultEncoding = defaultEncoding;
    }

    public void send(String subject, String message, String receiver) throws MessagingException {
        MimeMessageHelper messageHelper = new MimeMessageHelper(javaMailSender.createMimeMessage(), false, defaultEncoding);
        messageHelper.setFrom(mailAuthorAccount);
        messageHelper.setSubject(subject);
        messageHelper.setText(message, true);
        messageHelper.setTo(receiver);
        javaMailSender.send(messageHelper.getMimeMessage());
    }
}
