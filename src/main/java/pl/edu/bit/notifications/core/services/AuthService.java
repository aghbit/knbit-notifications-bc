package pl.edu.bit.notifications.core.services;

import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.edu.bit.notifications.rest.exception.ForbiddenException;
import pl.edu.bit.notifications.rest.exception.UnauthorizedException;
import pl.edu.bit.notifications.rest.json.AuthPermissionJson;
import pl.edu.bit.notifications.rest.json.UserIdJson;

@Data
@Component
@Log4j
public class AuthService {

    @Value("${service.auth.url}")
    private String authorizationUrl;

    @Value("${service.auth.header}")
    private String authHeader;

    @Value("${service.auth.permission}")
    private String permission;

    public void authorize(String authToken) throws UnauthorizedException, ForbiddenException {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add(authHeader, authToken);
            HttpEntity<AuthPermissionJson> entity = new HttpEntity<>(new AuthPermissionJson(permission), headers);
            restTemplate.exchange(authorizationUrl, HttpMethod.POST, entity, UserIdJson.class);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    throw new UnauthorizedException();
                case FORBIDDEN:
                    throw new ForbiddenException();
                default:
                    log.error("Request authorization failed for reason other than 401/403", e);
            }
        }
    }
}
