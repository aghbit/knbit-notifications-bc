package pl.edu.bit.notifications.core.mail;

import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.bit.notifications.rest.exception.UnauthorizedException;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Log4j
@Component
public class MailSendingExecutor extends ScheduledThreadPoolExecutor {

    public static final int DEFAULT_CORE_POOL_SIZE = 4;

    private static final int MAILS_BATCH_SIZE = 23;

    private static final TimeUnit TIME_DELAY_UNIT = TimeUnit.MINUTES;

    private final MailSender mailSender;

    @Autowired
    public MailSendingExecutor(MailSender mailSender) {
        super(DEFAULT_CORE_POOL_SIZE);
        this.mailSender = mailSender;
    }

    public void scheduleMail(MailRequest mailRequest, String authToken) throws UnauthorizedException {
        Lists.partition(mailRequest.getReceiversEmails(), MAILS_BATCH_SIZE).stream().forEach(smallerReceiverList -> {
            Runnable sendMailTask = () ->
                    smallerReceiverList.forEach(receiver -> sendMail(mailRequest.getSubject(), mailRequest.getMessage
                            (), receiver));
            schedule(sendMailTask, getTaskInProgressCount(), TIME_DELAY_UNIT);
        });
    }

    private void sendMail(String subject, String message, String receiver) {
        try {
            mailSender.send(subject, message, receiver);
        } catch (Exception e) {
            // Spring tends to throw unchecked exception when mail cannot be sent (i.e. bad recipient address),
            // so in order not to terminate the whole Runnable task -> all possible exceptions must be caught
            log.error(String.format("Failed to send mail '%s' to '%s'. Caused by ", subject, receiver), e);
        }
    }

    public long getTaskInProgressCount() {
        return getTaskCount() - getCompletedTaskCount();
    }

}
