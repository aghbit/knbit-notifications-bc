package pl.edu.bit.notifications.core.mail;

import lombok.Value;

import java.util.List;

@Value
public class MailRequest {
    private final String subject;
    private final String message;
    private final List<String> receiversEmails;
}
