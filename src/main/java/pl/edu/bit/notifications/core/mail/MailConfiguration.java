package pl.edu.bit.notifications.core.mail;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@Data
@ConfigurationProperties(prefix = "mail")
public class MailConfiguration {

    private String host;
    private String username;
    private String password;
    private String protocol;
    private int port;
    private String defaultEncoding;

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtps.auth", true);
        mailProperties.put("mail.smtp.ssl.enable", true);
        mailProperties.put("mail.transport.protocol", "smtps");
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost(host);
        mailSender.setUsername(username);
        mailSender.setPassword(password);
        mailSender.setProtocol(protocol);
        mailSender.setPort(port);
        mailSender.setDefaultEncoding(defaultEncoding);
        return mailSender;
    }

    @Bean
    public String mailAuthor() {
        return username;
    }

    @Bean
    String defaultEncoding() {
        return defaultEncoding;
    }
}
