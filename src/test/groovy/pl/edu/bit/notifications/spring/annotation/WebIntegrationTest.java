package pl.edu.bit.notifications.spring.annotation;

import org.springframework.test.context.ActiveProfiles;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

@org.springframework.boot.test.WebIntegrationTest("server.port:0")
@ActiveProfiles("test")
public @interface WebIntegrationTest {

}

