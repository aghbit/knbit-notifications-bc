KNBIT-NOTIFICATIONS
========

Requirements
------------
- JDK 8

Docker
------

### Building image from remote develop branch
`docker build -t <yourname>/knbit-notifications:dev --no-cache -f docker/Dockerfile .`